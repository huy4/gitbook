<h2 id="section23">404 Overview</h2>

<h4>Overview</h4>

- The main one is how you can make the snow an image
- Making the image stick at the bottom of the page no matter what device you are using
- It will always stick to the bottom of the page as we can see that design is very cool and simple as

<h4>Details</h4>

- It's just 404 page not found which you can use in your website as a 404.
- We have this 404 logo and text of button and image.
- Notice that the logo and the image are under the snow animation but the text and the button are above

<h4>Bookmark</h4>

![Image 404](./section23.PNG)

<h2 id="section24">404 Prepare File</h2>

<h4>Overview</h4>

- Prepare file for 404 Project 

<h4>Details</h4>

- Export favicon.png, background.png and 404-penguins.png
- Download Eurostile-font
- color used:
    - grey-color: #313f4c
    - blue-color: #1ca9f9
    - white-color: #fff

<h4>Bookmark</h4>

![Image 404](./section23.PNG)

<h2 id="section24">404 Start Code</h2>

<h4>Overview</h4>

- Make 404 page

<h4>Details</h4>

- CSS:
    - `z-index`: 
                - The `z-index` property specifies the stack order of an element. An element with greater stack order is always in front of an element with a lower stack order.
                - `z-index` only works on positioned elements (position:absolute, position:relative, or position:fixed).
    -  `@keyframes` rule specifiles the animation code.
    - The `position` property specifies the type of positioning method used for an element (static, relative, absolute, fixed, or sticky).
    - CSS `transitions` allows you to change property values smoothly (from one value to another), over a given duration.

<h4>Bookmark</h4>

- [z-index](https://www.w3schools.com/cssref/pr_pos_z-index.asp)
- [@keyframes](https://www.w3schools.com/cssref/css3_pr_animation-keyframes.asp)
- [Position](https://www.w3schools.com/cssref/pr_class_position.asp)
- [Transition](https://www.w3schools.com/css/css3_transitions.asp)

![Image 404](./section23.PNG)

<h2 id="section24">404 Recap</h2>

<h4>Overview</h4>

- Recap code 404 page progress

<h4>Details</h4>

- I had tested the 404 page before in different browsers and it works just fine.

<h4>Bookmark</h4>
